# Stage 1: Build
FROM golang:1.16.4 AS build
WORKDIR /build
COPY go.mod go.sum ./
RUN go mod download
COPY main.go .
RUN CGO_ENABLED=0 GOOS=linux go build -o issue-bot ./...

# Stage 2: Release
FROM alpine:3.9 AS release
RUN apk add ca-certificates
COPY --from=build /build/issue-bot /issue-bot
ENTRYPOINT ["/issue-bot"]
